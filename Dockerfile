FROM ibmfunctions/action-nodejs-v8

RUN (curl -fsSL https://clis.cloud.ibm.com/install/linux | sh) && \
ln -s /usr/local/bin/ibmcloud /usr/local/bin/ic && \
ic api https://cloud.ibm.com && \
wget -qO - http://packages.confluent.io/deb/4.0/archive.key | apt-key add - && \
apt-get update && apt-get install software-properties-common -y && \
add-apt-repository "deb [arch=amd64] http://packages.confluent.io/deb/4.0 stable main" && \
apt-get update && apt-get install zip -y && \
apt-get remove software-properties-common -y && \
apt-get autoremove -y && \
apt-get clean && \
apt-get autoclean && \
rm -rf /var/lib/apt/lists/* && \
rm -rf /usr/share/man/?? && \
rm -rf /usr/share/man/??_* && \
rm -rf /var/cache/* && \
rm -rf /tmp/*
